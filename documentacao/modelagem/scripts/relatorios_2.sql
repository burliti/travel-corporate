﻿SELECT
     l.data_inicial,
     c.nome as categoria,
     SUM(l.valor_lancamento * CASE WHEN l.tipo_lancamento = 'D' THEN 1 ELSE 0 END) as valor
  FROM viagem v
  JOIN lancamento l ON l.id_viagem = v.id_viagem
  JOIN funcionario f ON f.id_funcionario = v.id_funcionario
  JOIN categoria c ON c.id_categoria = l.id_categoria
  JOIN empresa e ON e.id_empresa = v.id_empresa
  LEFT JOIN empresa e2 ON e2.id_empresa = f.id_empresa_filial
  LEFT JOIN setor s ON s.id_setor = f.id_setor
  LEFT JOIN projeto p ON p.id_projeto = v.id_projeto
  LEFT JOIN cliente cl ON cl.id_cliente = v.id_cliente
  
  LEFT JOIN centro_custo c1 ON c1.id_centro_custo = e.id_centro_custo
  LEFT JOIN centro_custo c2 ON c2.id_centro_custo = e2.id_centro_custo
  LEFT JOIN centro_custo c3 ON c3.id_centro_custo = s.id_centro_custo
  LEFT JOIN centro_custo c4 ON c4.id_centro_custo = cl.id_centro_custo
  LEFT JOIN centro_custo c5 ON c5.id_centro_custo = p.id_centro_custo
  
 WHERE 1 = 1
   AND v.id_empresa = 1
   AND l.tipo_lancamento = 'D'
   AND c.flag_interna = 'N'
   
   -- Filtros
   AND (f.id_empresa_filial = NULL OR NULL IS NULL)
   AND (s.id_setor = NULL OR NULL IS NULL)
   AND (f.id_funcionario = NULL OR NULL IS NULL)
   AND (v.status = NULL OR NULL IS NULL)
   AND (c.id_categoria = NULL OR NULL IS NULL)
   AND (l.data_inicial >= NULL OR NULL IS NULL)
   AND (l.data_inicial <= NULL OR NULL IS NULL)
   AND (c1.id_centro_custo = NULL OR 
        c2.id_centro_custo = NULL OR 
        c3.id_centro_custo = NULL OR 
        c4.id_centro_custo = NULL OR
        c5.id_centro_custo = NULL OR              
        NULL IS NULL)
   
   -- Fim Filtros
 GROUP BY c.nome, l.data_inicial, e.id_empresa, e.nome, e.logo
 ORDER BY l.data_inicial, c.nome

