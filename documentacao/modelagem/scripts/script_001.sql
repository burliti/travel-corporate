﻿/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-09-24 23:05                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Add sequences                                                          */
/* ---------------------------------------------------------------------- */
CREATE SEQUENCE public.seq_empresa INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_funcionario INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_funcao INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_viagem INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_projeto INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_cliente INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_sessao_usuario INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_sessao_usuario_excluida INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_lancamento INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_recibo INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_categoria INCREMENT 1 START 1;

/* ---------------------------------------------------------------------- */
/* Add domains                                                            */
/* ---------------------------------------------------------------------- */
CREATE DOMAIN public.id AS NUMERIC(8);
CREATE DOMAIN public.status AS CHARACTER(1) CHECK (VALUE IN ('A', 'I'));
CREATE DOMAIN public.sim_nao AS CHARACTER(1) CHECK (VALUE IN ('S', 'N'));
CREATE DOMAIN public.tipo_sessao AS NUMERIC(1) CHECK (VALUE IN (1, 2, 3));
CREATE DOMAIN public.tipo_lancamento AS CHARACTER(1) CHECK (VALUE IN ('C', 'D'));

/* ---------------------------------------------------------------------- */
/* Add table "empresa"                                                    */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.empresa (
    id_empresa public.id  NOT NULL,
    nome CHARACTER VARYING(150)  NOT NULL,
    fantasia CHARACTER VARYING(200),
    id_empresa_principal public.id,
    principal public.sim_nao  NOT NULL,
    chave_acesso CHARACTER VARYING(150)  NOT NULL,
    CONSTRAINT PK_empresa PRIMARY KEY (id_empresa)
);

/* ---------------------------------------------------------------------- */
/* Add table "funcionario"                                                */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.funcionario (
    id_funcionario public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    id_funcao public.id  NOT NULL,
    nome CHARACTER VARYING(150)  NOT NULL,
    email CHARACTER VARYING(300)  NOT NULL,
    senha CHARACTER VARYING(100)  NOT NULL,
    status public.status  NOT NULL,
    celular CHARACTER VARYING(20),
    administrador public.sim_nao  NOT NULL,
    resetar_senha public.sim_nao  NOT NULL,
    CONSTRAINT PK_funcionario PRIMARY KEY (id_funcionario)
);

/* ---------------------------------------------------------------------- */
/* Add table "funcao"                                                     */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.funcao (
    id_funcao public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    nome CHARACTER VARYING(100)  NOT NULL,
    status public.status  NOT NULL,
    CONSTRAINT PK_funcao PRIMARY KEY (id_funcao)
);

/* ---------------------------------------------------------------------- */
/* Add table "sessao_usuario"                                             */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.sessao_usuario (
    id_sessao public.id  NOT NULL,
    id_empresa NUMERIC(8)  NOT NULL,
    id_funcionario NUMERIC(8)  NOT NULL,
    chave_sessao CHARACTER VARYING(50)  NOT NULL,
    data_criacao TIMESTAMP  NOT NULL,
    ultima_interacao TIMESTAMP  NOT NULL,
    endereco_ip CHARACTER VARYING(200)  NOT NULL,
    hostname CHARACTER VARYING(200)  NOT NULL,
    tipo_sessao public.tipo_sessao,
    CONSTRAINT PK_sessao_usuario PRIMARY KEY (id_sessao)
);
ALTER TABLE public.sessao_usuario ADD CONSTRAINT TUC_sessao_usuario_1 
    UNIQUE (chave_sessao);

/* ---------------------------------------------------------------------- */
/* Add table "sessao_usuario_excluida"                                    */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.sessao_usuario_excluida (
    id_sessao_excluida public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    id_funcionario public.id  NOT NULL,
    id_sessao_original public.id  NOT NULL,
    chave_sessao CHARACTER VARYING(50),
    data_criacao TIMESTAMP  NOT NULL,
    ultima_interacao TIMESTAMP  NOT NULL,
    data_logoff TIMESTAMP  NOT NULL,
    forma_logoff NUMERIC(1)  NOT NULL,
    endereco_ip CHARACTER VARYING(200)  NOT NULL,
    hostname CHARACTER VARYING(200)  NOT NULL,
    CONSTRAINT PK_sessao_usuario_excluida PRIMARY KEY (id_sessao_excluida)
);

/* ---------------------------------------------------------------------- */
/* Add table "projeto"                                                    */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.projeto (
    id_projeto public.id  NOT NULL,
    id_empresa NUMERIC(8)  NOT NULL,
    nome CHARACTER VARYING(200)  NOT NULL,
    status public.status  NOT NULL,
    valor NUMERIC(14,2),
    percentual_limite_gastos NUMERIC(5,2),
    percentual_alerta_gastos NUMERIC(5,2),
    CONSTRAINT PK_projeto PRIMARY KEY (id_projeto)
);

/* ---------------------------------------------------------------------- */
/* Add table "cliente"                                                    */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.cliente (
    id_cliente public.id  NOT NULL,
    id_empresa NUMERIC(8)  NOT NULL,
    nome CHARACTER VARYING(200)  NOT NULL,
    status public.status  NOT NULL,
    CONSTRAINT PK_cliente PRIMARY KEY (id_cliente)
);

/* ---------------------------------------------------------------------- */
/* Add table "cliente_projeto"                                            */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.cliente_projeto (
    id_cliente NUMERIC(8)  NOT NULL,
    id_projeto NUMERIC(8)  NOT NULL,
    CONSTRAINT PK_cliente_projeto PRIMARY KEY (id_cliente, id_projeto)
);

/* ---------------------------------------------------------------------- */
/* Add table "viagem"                                                     */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.viagem (
    id_viagem public.id  NOT NULL,
    id_empresa NUMERIC(8)  NOT NULL,
    objetivo CHARACTER VARYING(500),
    descricao CHARACTER VARYING(150),
    observacao TEXT,
    id_cliente NUMERIC(8),
    id_projeto NUMERIC(8),
    data_ida DATE,
    data_volta DATE,
    status NUMERIC(1)  NOT NULL,
    CONSTRAINT PK_viagem PRIMARY KEY (id_viagem)
);

/* ---------------------------------------------------------------------- */
/* Add table "funcionario_viagem"                                         */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.funcionario_viagem (
    id_funcionario NUMERIC(8)  NOT NULL,
    id_viagem NUMERIC(8)  NOT NULL,
    CONSTRAINT PK_funcionario_viagem PRIMARY KEY (id_funcionario, id_viagem)
);

/* ---------------------------------------------------------------------- */
/* Add table "lancamento"                                                 */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.lancamento (
    id_lancamento public.id  NOT NULL,
    id_empresa NUMERIC(8)  NOT NULL,
    id_funcionario NUMERIC(8)  NOT NULL,
    id_viagem NUMERIC(8),
    id_recibo NUMERIC(8),
    tipo_lancamento public.tipo_lancamento  NOT NULL,
    valor_lancamento NUMERIC(15,2),
    data_hora_lancamento TIMESTAMP  NOT NULL,
    data_referencia DATE  NOT NULL,
    observacoes CHARACTER VARYING(500),
    id_categoria NUMERIC(8),
    CONSTRAINT PK_lancamento PRIMARY KEY (id_lancamento)
);

/* ---------------------------------------------------------------------- */
/* Add table "recibo"                                                     */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.recibo (
    id_recibo public.id  NOT NULL,
    id_empresa NUMERIC(8)  NOT NULL,
    id_funcionario NUMERIC(8)  NOT NULL,
    numero_documento CHARACTER VARYING(200),
    data_referencia DATE  NOT NULL,
    foto_anexo_recibo BYTEA,
    data_inserido TIMESTAMP  NOT NULL,
    CONSTRAINT PK_recibo PRIMARY KEY (id_recibo)
);

/* ---------------------------------------------------------------------- */
/* Add table "categoria"                                                  */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.categoria (
    id_categoria public.id  NOT NULL,
    id_empresa NUMERIC(8)  NOT NULL,
    nome CHARACTER VARYING(150)  NOT NULL,
    status public.status  NOT NULL,
    flag_interna public.sim_nao  NOT NULL,
    tipo_lancamento_categoria public.tipo_lancamento,
    CONSTRAINT PK_categoria PRIMARY KEY (id_categoria)
);
COMMENT ON COLUMN public.categoria.flag_interna IS 'Indica se a categoria é interna e não pode ser alterada.';

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.empresa ADD CONSTRAINT empresa_empresa 
    FOREIGN KEY (id_empresa_principal) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.funcionario ADD CONSTRAINT empresa_funcionario 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.funcionario ADD CONSTRAINT funcao_funcionario 
    FOREIGN KEY (id_funcao) REFERENCES public.funcao (id_funcao);
ALTER TABLE public.funcao ADD CONSTRAINT empresa_funcao 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.sessao_usuario ADD CONSTRAINT empresa_sessao_usuario 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.sessao_usuario ADD CONSTRAINT funcionario_sessao_usuario 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.sessao_usuario_excluida ADD CONSTRAINT empresa_sessao_usuario_excluida 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.sessao_usuario_excluida ADD CONSTRAINT funcionario_sessao_usuario_excluida 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.projeto ADD CONSTRAINT empresa_projeto 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.cliente ADD CONSTRAINT empresa_cliente 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.cliente_projeto ADD CONSTRAINT cliente_cliente_projeto 
    FOREIGN KEY (id_cliente) REFERENCES public.cliente (id_cliente);
ALTER TABLE public.cliente_projeto ADD CONSTRAINT projeto_cliente_projeto 
    FOREIGN KEY (id_projeto) REFERENCES public.projeto (id_projeto);
ALTER TABLE public.viagem ADD CONSTRAINT cliente_viagem 
    FOREIGN KEY (id_cliente) REFERENCES public.cliente (id_cliente);
ALTER TABLE public.viagem ADD CONSTRAINT projeto_viagem 
    FOREIGN KEY (id_projeto) REFERENCES public.projeto (id_projeto);
ALTER TABLE public.viagem ADD CONSTRAINT empresa_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.funcionario_viagem ADD CONSTRAINT funcionario_funcionario_viagem 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.funcionario_viagem ADD CONSTRAINT viagem_funcionario_viagem 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.lancamento ADD CONSTRAINT viagem_lancamento 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.lancamento ADD CONSTRAINT funcionario_lancamento 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.lancamento ADD CONSTRAINT empresa_lancamento 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.lancamento ADD CONSTRAINT recibo_lancamento 
    FOREIGN KEY (id_recibo) REFERENCES public.recibo (id_recibo);
ALTER TABLE public.lancamento ADD CONSTRAINT categoria_lancamento 
    FOREIGN KEY (id_categoria) REFERENCES public.categoria (id_categoria);
ALTER TABLE public.recibo ADD CONSTRAINT funcionario_recibo 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.recibo ADD CONSTRAINT empresa_recibo 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.categoria ADD CONSTRAINT empresa_categoria 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
