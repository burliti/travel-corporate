﻿/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-09-29 13:31                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Modify table "sessao_usuario_excluida"                                 */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.sessao_usuario_excluida ADD
    observacoes CHARACTER VARYING(1000);
