/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-10-15 14:07                                */
/* ---------------------------------------------------------------------- */



/* ---------------------------------------------------------------------- */
/* Modify table "recibo"                                                  */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.recibo ADD
    url_nfe CHARACTER VARYING(2000);

