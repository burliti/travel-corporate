/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-10-15 18:56                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento_1;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento_2;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento_3;
ALTER TABLE public.fechamento DROP CONSTRAINT empresa_fechamento;

/* ---------------------------------------------------------------------- */
/* Modify table "fechamento"                                              */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.fechamento ADD
    id_funcionario_rejeicao public.id;
ALTER TABLE public.fechamento ALTER COLUMN id_empresa TYPE public.id;
ALTER TABLE public.fechamento ALTER COLUMN id_funcionario_fechamento TYPE public.id;
ALTER TABLE public.fechamento ALTER COLUMN id_funcionario_recebimento TYPE public.id;
ALTER TABLE public.fechamento ALTER COLUMN id_funcionario_aprovacao TYPE public.id;

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_1 
    FOREIGN KEY (id_funcionario_fechamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_2 
    FOREIGN KEY (id_funcionario_recebimento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_3 
    FOREIGN KEY (id_funcionario_aprovacao) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT empresa_fechamento 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento 
    FOREIGN KEY (id_funcionario_rejeicao) REFERENCES public.funcionario (id_funcionario);
