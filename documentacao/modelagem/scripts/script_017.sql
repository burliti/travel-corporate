/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-10-22 10:50                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.viagem DROP CONSTRAINT cliente_viagem;
ALTER TABLE public.viagem DROP CONSTRAINT projeto_viagem;
ALTER TABLE public.viagem DROP CONSTRAINT empresa_viagem;
ALTER TABLE public.viagem DROP CONSTRAINT funcionario_viagem;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento_1;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento_2;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento_3;
ALTER TABLE public.fechamento DROP CONSTRAINT empresa_fechamento;

/* ---------------------------------------------------------------------- */
/* Add domains                                                            */
/* ---------------------------------------------------------------------- */
CREATE DOMAIN public.status_viagem AS NUMERIC(1) CHECK (VALUE IN (1, 2, 3, 4, 5, 6, 7, 8));

/* ---------------------------------------------------------------------- */
/* Modify table "viagem"                                                  */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.viagem ALTER COLUMN status TYPE public.status_viagem;

/* ---------------------------------------------------------------------- */
/* Modify table "fechamento"                                              */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.fechamento DROP status;
DROP DOMAIN public.status_fechamento;

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.viagem ADD CONSTRAINT cliente_viagem 
    FOREIGN KEY (id_cliente) REFERENCES public.cliente (id_cliente);
ALTER TABLE public.viagem ADD CONSTRAINT projeto_viagem 
    FOREIGN KEY (id_projeto) REFERENCES public.projeto (id_projeto);
ALTER TABLE public.viagem ADD CONSTRAINT empresa_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.viagem ADD CONSTRAINT funcionario_viagem 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.lancamento ADD CONSTRAINT viagem_lancamento 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_1 
    FOREIGN KEY (id_funcionario_fechamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_2 
    FOREIGN KEY (id_funcionario_recebimento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_3 
    FOREIGN KEY (id_funcionario_aprovacao) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT empresa_fechamento 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento 
    FOREIGN KEY (id_funcionario_rejeicao) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT viagem_fechamento 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
