/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-01-28 23:06                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.assunto DROP CONSTRAINT empresa_assunto;
ALTER TABLE public.mensagem DROP CONSTRAINT assunto_mensagem;

/* ---------------------------------------------------------------------- */
/* Drop and recreate table "assunto"                                      */
/* ---------------------------------------------------------------------- */

/* Table must be recreated because some of the changes can't be done with the regular commands available. */
ALTER TABLE public.assunto DROP CONSTRAINT pk_assunto;
CREATE TABLE public.assunto_TMP (
    id_assunto public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    nome CHARACTER VARYING(100)  NOT NULL,
    status public.status  NOT NULL,
    flg_permite_responder public.sim_nao  NOT NULL,
    flg_permite_encaminhar public.sim_nao  NOT NULL,
    flg_exige_confirmacao public.sim_nao  NOT NULL,
    flg_bloqueia_sem_responder public.sim_nao  NOT NULL,
    texto_botao_confirmacao CHARACTER VARYING(50));
INSERT INTO public.assunto_TMP
    (id_assunto,id_empresa,nome,status,flg_permite_responder,flg_exige_confirmacao,flg_bloqueia_sem_responder)
SELECT
    id_assunto,id_empresa,nome,status,flg_permite_responder,flg_exige_confirmacao,flg_bloqueia_sem_responder
FROM public.assunto;

DROP TABLE public.assunto;
ALTER TABLE public.assunto_TMP RENAME TO assunto;
ALTER TABLE public.assunto ADD CONSTRAINT pk_assunto 
    PRIMARY KEY (id_assunto);

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.assunto ADD CONSTRAINT empresa_assunto 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.mensagem ADD CONSTRAINT assunto_mensagem 
    FOREIGN KEY (id_assunto) REFERENCES public.assunto (id_assunto);

