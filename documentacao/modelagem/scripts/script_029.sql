/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-01-28 23:21                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.mensagem DROP CONSTRAINT empresa_mensagem;
ALTER TABLE public.mensagem DROP CONSTRAINT funcionario_mensagem;
ALTER TABLE public.mensagem DROP CONSTRAINT mensagem_mensagem_2;
ALTER TABLE public.mensagem DROP CONSTRAINT mensagem_mensagem_1;
ALTER TABLE public.mensagem DROP CONSTRAINT assunto_mensagem;

/* ---------------------------------------------------------------------- */
/* Modify table "mensagem"                                                */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.mensagem ALTER COLUMN data_hora_criacao TYPE TIMESTAMP;
ALTER TABLE public.mensagem ALTER COLUMN data_hora_envio TYPE TIMESTAMP;
ALTER TABLE public.mensagem ALTER COLUMN data_hora_exclusao TYPE TIMESTAMP;

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.mensagem ADD CONSTRAINT empresa_mensagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.mensagem ADD CONSTRAINT funcionario_mensagem 
    FOREIGN KEY (id_funcionario_autor) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.mensagem ADD CONSTRAINT assunto_mensagem 
    FOREIGN KEY (id_assunto) REFERENCES public.assunto (id_assunto);
ALTER TABLE public.mensagem ADD CONSTRAINT mensagem_mensagem_2 
    FOREIGN KEY (id_mensagem_origem_resposta) REFERENCES public.mensagem (id_mensagem);
ALTER TABLE public.mensagem ADD CONSTRAINT mensagem_mensagem_1 
    FOREIGN KEY (id_mensagem_origem_encaminhada) REFERENCES public.mensagem (id_mensagem);
