/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-01-30 07:27                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.mensagem_destinatarios DROP CONSTRAINT mensagem_mensagem_destinatarios;
ALTER TABLE public.mensagem_destinatarios DROP CONSTRAINT funcionario_mensagem_destinatarios;
ALTER TABLE public.mensagem_destinatarios DROP CONSTRAINT empresa_mensagem_destinatarios;

/* ---------------------------------------------------------------------- */
/* Add domains                                                            */
/* ---------------------------------------------------------------------- */
CREATE DOMAIN public.status_mensagem_destinatario AS NUMERIC(1) CHECK (VALUE IN (1, 2, 9));

/* ---------------------------------------------------------------------- */
/* Modify table "mensagem_destinatarios"                                  */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.mensagem_destinatarios ALTER COLUMN status TYPE public.status_mensagem_destinatario;

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.mensagem_destinatarios ADD CONSTRAINT mensagem_mensagem_destinatarios 
    FOREIGN KEY (id_mensagem) REFERENCES public.mensagem (id_mensagem);
ALTER TABLE public.mensagem_destinatarios ADD CONSTRAINT funcionario_mensagem_destinatarios 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.mensagem_destinatarios ADD CONSTRAINT empresa_mensagem_destinatarios 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
