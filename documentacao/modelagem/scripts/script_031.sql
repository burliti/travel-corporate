/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-01-31 07:32                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Add sequences                                                          */
/* ---------------------------------------------------------------------- */
CREATE SEQUENCE public.seq_multa INCREMENT 1 START 1;

/* ---------------------------------------------------------------------- */
/* Add domains                                                            */
/* ---------------------------------------------------------------------- */
CREATE DOMAIN public.tipo_veiculo AS NUMERIC CHECK (VALUE IN (1, 2, 3));

/* ---------------------------------------------------------------------- */
/* Add table "multa"                                                      */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.multa (
    id_multa public.id  NOT NULL,
    id_empresa public.id,
    id_funcionario_multa public.id,
    id_funcionario_cadastro public.id,
    numero CHARACTER VARYING(50),
    data_hora_multa TIMESTAMP,
    cidade_ocorrencia CHARACTER VARYING(100),
    uf_multa CHARACTER(2),
    codigo_multa CHARACTER VARYING(50),
    texto_multa CHARACTER VARYING(200),
    valor_multa NUMERIC(10,2),
    tipo_veiculo public.tipo_veiculo  NOT NULL,
    CONSTRAINT pk_multa PRIMARY KEY (id_multa)
);

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.multa ADD CONSTRAINT empresa_multa 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.multa ADD CONSTRAINT funcionario_multa_2 
    FOREIGN KEY (id_funcionario_multa) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.multa ADD CONSTRAINT funcionario_multa_3 
    FOREIGN KEY (id_funcionario_cadastro) REFERENCES public.funcionario (id_funcionario);
