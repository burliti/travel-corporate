export USERADMIN=orlando
export PASSADMIN=PutaQuePariu18
export HOME_PROJETOS=/home/wildfly/projetos/travel-corporate-java-api/
export VERSAO=1.2.0

[ -d "$HOME_PROJETOS/deploy" ] || mkdir $HOME_PROJETOS/deploy

cd $HOME_PROJETOS/deploy

#Limpa projeto
rm -Rf 

cd $HOME_PROJETOS/all

#Build do projeto
mvn clean package install

#Deploy do projeto
cd $HOME_PROJETOS/deploy


#Deploy da api RESOURCES
export WAR=travelcorporate-web-api-$VERSAO.war
export UNDEPLOY=travelcorporate-web-api-*

echo Realizando undeploy....
jboss-cli.sh -c controller=localhost:9990 --user=$USERADMIN --password=$PASSADMIN --timeout=10000 --command="undeploy $UNDEPLOY"
echo Aplicacao removida.

echo Publicando $WAR...
jboss-cli.sh -c controller=localhost:9990 --user=$USERADMIN --password=$PASSADMIN --timeout=10000 --command="deploy $WAR"
echo Publicado.

#Deploy da api REPORTS
export WAR=travelcorporate-report-api-$VERSAO.war
export UNDEPLOY=travelcorporate-report-api-*

echo Realizando undeploy....
jboss-cli.sh -c controller=localhost:9990 --user=$USERADMIN --password=$PASSADMIN --timeout=10000 --command="undeploy $UNDEPLOY"
echo Aplicacao removida.

echo Publicando $WAR...
jboss-cli.sh -c controller=localhost:9990 --user=$USERADMIN --password=$PASSADMIN --timeout=10000 --command="deploy $WAR"
echo Publicado.


#Deploy da api MESSAGES
export WAR=travelcorporate-messages-api-$VERSAO.war
export UNDEPLOY=travelcorporate-messages-api-*

echo Realizando undeploy....
jboss-cli.sh -c controller=localhost:9990 --user=$USERADMIN --password=$PASSADMIN --timeout=10000 --command="undeploy $UNDEPLOY"
echo Aplicacao removida.

echo Publicando $WAR...
jboss-cli.sh -c controller=localhost:9990 --user=$USERADMIN --password=$PASSADMIN --timeout=10000 --command="deploy $WAR"
echo Publicado.