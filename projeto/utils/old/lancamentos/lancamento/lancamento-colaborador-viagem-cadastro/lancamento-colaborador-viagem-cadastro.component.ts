import { TituloPaginaService } from './../../../../components/services/titulo-pagina.service';
import { TirarFotoService } from './../../../../plugins/camera/tirar-foto.service';
import { LoginService } from './../../../../security/login.service';
import { ViagemService } from './../../viagem/viagem.service';
import { Viagem } from './../../viagem/viagem.model';
import { CategoriaService } from './../../../configuracoes/categoria/categoria.service';
import { LancamentoService } from './../lancamento.service';
import { Lancamento } from './../lancamento.model';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

declare var $ : any;

@Component({
  selector: 'app-lancamento-colaborador-viagem-cadastro',
  templateUrl: './lancamento-colaborador-viagem-cadastro.component.html',
  styleUrls: ['./lancamento-colaborador-viagem-cadastro.component.css']
})
export class LancamentoColaboradorViagemCadastroComponent extends AbstractCadastroComponent<Lancamento, LancamentoService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensCategoria: SelectGroupOption[] = [];

  viagem: Viagem = {};

  constructor(
    private tituloPaginaService : TituloPaginaService,
    private tirarFotoService: TirarFotoService,
    private loginService: LoginService,
    private route: ActivatedRoute,
    private router: Router,
    private categoriaService: CategoriaService,
    private lancamentoService: LancamentoService,
    private viagemService: ViagemService,
    private msg: MensagemService) {
    super(route, router, msg);
  }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Cadastro de Lançamentos da viagem");

    const s = this.categoriaService.getListAtivos().subscribe((retorno) => {
      this.itensCategoria.push({ label: 'Selecione a categoria de lançamento', value: '' });

      for (const categoria of retorno.lista) {
        this.itensCategoria.push({ label: categoria.nome, value: categoria.id, group: categoria.tipo });
      }

      const s2 = this.route.params.subscribe((params: any[]) => {
        let idViagem = params['idViagem'];

        if (idViagem) {
          const s3 = this.viagemService.buscar(idViagem).subscribe((retorno) => {
            this.viagem = retorno;
          });
          this.inscricoes.push(s3);

          super.ngOnInit();
        }
      });
      this.inscricoes.push(s2);
    });
    this.inscricoes.push(s);
  }

  getService(): LancamentoService {
    return this.lancamentoService;
  }

  salvar() {
    this.vo.viagem = this.viagem;
    super.salvar();
  }

  voltar() {
    this.router.navigate(['lancamentos/minhas-viagens', this.viagem.id]);
  }

  selecionarRecibo() {
    this.msg.warn("Seleção de recibo ainda nao implementada!");
  }

  novoRecibo() {
    this.vo.recibo = {
      id: "",
      funcionario: {}
    };

    setTimeout(() => $("#internal_recibo_dataReferencia").focus(), 300);
  }

  limparRecibo() {
    this.vo.recibo.id = "-1";
  }

  isPodeEditarRecibo(): boolean {
    if (this.isExcluir()) {
      // Se estiver em modo de exclusão, não pode editar também.
      return false;
    } else if (!this.vo.recibo) {
      // Se nao tiver recibo, nao pode editar
      return false;
    } else if (!this.vo.recibo.id) {
      // Se for um recibo novo, pode editar
      return true;
    } else if (this.vo.recibo.funcionario && this.vo.recibo.funcionario.id == this.loginService.getUsuarioAutenticado().funcionario.id) {
      // Se for um recibo cadastrado por mim, pode editar
      return true;
    }
    return false;
  }

  /**
   * Vai indicar se a tela de cadastro do recibo será exibida
   */
  isExibeRecibo(): boolean {
    return this.vo && this.vo.recibo && this.vo.recibo.id != "-1";
  }

  getVoNewInstance(): Lancamento {
    return {
      viagem: this.viagem,
      categoria: {},
      funcionario: {},
      recibo: {
        id: "-1",
        funcionario: {}
      }
    };
  }

  getFoto() {
    if (!this.vo.recibo.fotoAnexoRecibo || this.vo.recibo.fotoAnexoRecibo === '') {
      return 'assets/img/sem_imagem.png';
    } else {
      return this.vo.recibo.fotoAnexoRecibo;
    }
  }

  tirarFoto() {
    const idVideoSource = this.tirarFotoService.retornarDeviceFoto('cadastro_morador');

    this.tirarFotoService.tirarFoto(idVideoSource)
      .then((fotoBase64) => {
        this.vo.recibo.fotoAnexoRecibo = fotoBase64;
      }).catch((mensagem) => {
        if (mensagem) {
          this.msg.nofiticationError(mensagem);
        }
      });
  }

  limparFoto() {
    this.vo.recibo.fotoAnexoRecibo = '';
  }
}
