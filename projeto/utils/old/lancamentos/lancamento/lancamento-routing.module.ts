import { LancamentoColaboradorViagemCadastroComponent } from './lancamento-colaborador-viagem-cadastro/lancamento-colaborador-viagem-cadastro.component';
import { MinhasViagensComponent } from './minhas-viagens/minhas-viagens.component';
import { LancamentoColaboradorViagemComponent } from './lancamento-colaborador-viagem/lancamento-colaborador-viagem.component';
import { LancamentoCadastroComponent } from './lancamento-cadastro/lancamento-cadastro.component';
import { LancamentoConsultaComponent } from './lancamento-consulta/lancamento-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { LancamentoComponent } from './lancamento.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'lancamentos/lancamento', component: LancamentoComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: LancamentoConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: LancamentoCadastroComponent },
        { path: ':id', component: LancamentoCadastroComponent }
      ]
    }
  ]
},
{
  path: 'lancamentos/minhas-viagens', component: MinhasViagensComponent, canActivate: [AutenticacaoGuard]
},
{
  path: 'lancamentos/minhas-viagens/:idViagem', component: LancamentoColaboradorViagemComponent, canActivate: [AutenticacaoGuard]
},
{
  path: 'lancamentos/minhas-viagens/:idViagem/:acao', component: LancamentoColaboradorViagemCadastroComponent, canActivate: [AutenticacaoGuard]
},
{
  path: 'lancamentos/minhas-viagens/:idViagem/:acao/:id', component: LancamentoColaboradorViagemCadastroComponent, canActivate: [AutenticacaoGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LancamentoRoutingModule { }
