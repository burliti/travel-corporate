import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lancamento',
  templateUrl: './lancamento.component.html',
  styleUrls: ['./lancamento.component.css']
})
export class LancamentoComponent implements OnInit {

  constructor(private tituloPaginaService: TituloPaginaService) { }

    ngOnInit() {
      this.tituloPaginaService.setTitulo("Lançamentos");
    }


}
