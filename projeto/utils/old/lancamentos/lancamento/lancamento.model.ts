import { Categoria } from './../../configuracoes/categoria/categoria.model';
import { Funcionario } from './../../cadastros/funcionario/funcionario.model';
import { Viagem } from '../../financeiro/viagem/viagem.model';
import { Recibo } from '../../financeiro/recibo/recibo.model';

export class Lancamento {
  id?: string;
  dataHoraLancamento?: string;
  dataReferencia?: string;
  observacoes?: string;
  tipoLancamento?: string;
  valorLancamento?: number;
  categoria?: Categoria = {};
  funcionario?: Funcionario = {};
  recibo?: Recibo = {};
  viagem?: Viagem = {};
}
