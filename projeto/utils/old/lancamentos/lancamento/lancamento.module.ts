import { LancamentoService } from './lancamento.service';
import { CategoriaModule } from './../../configuracoes/categoria/categoria.module';
import { ReciboModule } from './../recibo/recibo.module';
import { FormsModule } from '@angular/forms';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LancamentoRoutingModule } from './lancamento-routing.module';
import { LancamentoComponent } from './lancamento.component';
import { LancamentoCadastroComponent } from './lancamento-cadastro/lancamento-cadastro.component';
import { LancamentoConsultaComponent } from './lancamento-consulta/lancamento-consulta.component';
import { LancamentoColaboradorViagemComponent } from './lancamento-colaborador-viagem/lancamento-colaborador-viagem.component';
import { MinhasViagensComponent } from './minhas-viagens/minhas-viagens.component';
import { LancamentoColaboradorViagemCadastroComponent } from './lancamento-colaborador-viagem-cadastro/lancamento-colaborador-viagem-cadastro.component';

@NgModule({
  imports: [
    CommonModule,
    PluginsModule,
    FormsModule,
    ReciboModule,
    CategoriaModule,
    LancamentoRoutingModule
  ],
  providers: [LancamentoService],
  declarations: [LancamentoComponent, LancamentoCadastroComponent, LancamentoConsultaComponent, LancamentoColaboradorViagemComponent, MinhasViagensComponent, LancamentoColaboradorViagemCadastroComponent]
})
export class LancamentoModule { }
