import { TituloPaginaService } from './../../../../components/services/titulo-pagina.service';
import { ViagemService } from './../../viagem/viagem.service';
import { Viagem } from './../../viagem/viagem.model';
import { ClienteService } from './../../../cadastros/cliente/cliente.service';
import { DataPaginationEvent } from './../../../../plugins/plugins.api';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EnumUtils } from './../../../../plugins/shared';
import { Subscription } from 'rxjs/Rx';
import { DataTableComponent } from './../../../../plugins/data-table/data-table.component';
import { ConsultaResponse } from './../../../../components/model/consulta-response.model';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-minhas-viagens',
  templateUrl: './minhas-viagens.component.html',
  styleUrls: ['./minhas-viagens.component.css']
})
export class MinhasViagensComponent implements OnDestroy, AfterViewInit {
  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo("TODOS");
  itensStatusViagem: SelectGroupOption[] = EnumUtils.getOptionsStatusViagem("TODOS");

  dados: ConsultaResponse<Viagem>;

  filtro: any = {
    "ativo": ""
  };
  order: any = { "e.dataIda": "desc" };

  private inscricoes: Subscription[] = [];

  // Recupera o componente dataTable
  @ViewChild(DataTableComponent) dt: DataTableComponent<Viagem>;

  constructor(
    private tituloPaginaService : TituloPaginaService,
    private route: ActivatedRoute,
    private router: Router,
    private msg: MensagemService,
    private viagemService: ViagemService,
    private clienteService: ClienteService) { }

  ngAfterViewInit() { }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Minhas viagens");
  }

  ngOnDestroy() {
    for (let s of this.inscricoes) {
      s.unsubscribe();
    }
  }

  lancamentos() {
    if (this.dt.getSelectedItem()) {
      this.router.navigate([this.dt.getSelectedItem().id], { relativeTo: this.route });
    } else {
      this.msg.warn("Nenhuma viagem selecionada");
    }
  }

  pesquisar() {
    this.dt.load(1);
  }

  limpar() {
    this.filtro = {};
    this.pesquisar();
  }

  paginacao(event: DataPaginationEvent) {
    let s = this.viagemService.getListMinhasViagens({
      filtro: this.filtro,
      order: this.order,
      pageSize: event.pageSize,
      pageNumber: event.page
    }).subscribe(dados => this.dados = dados);
  }
}
