export HOME_PROJETOS=/home/wildfly/projetos/travel-corporate-app
export HOME_APACHE_APP=/home/travelcorporate/www/app

# Dummy
cd /home/wildfly/temp


if [ -d "$HOME_PROJETOS/dist" ]; then
	echo Limpando web-app...
	rm -Rf $HOME_APACHE_APP/*

	cd $HOME_PROJETOS/dist

	echo Movendo tudo para $HOME_APACHE_APP...
	mv * $HOME_APACHE_APP

	echo Publicado.
else
	echo Nao encontrado.
fi