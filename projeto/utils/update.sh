git config --global user.name "Orlando Burli Junior"
git config --global user.email orlando.burli@gmail.com

cd /home/wildfly/projetos/travel-corporate

git pull

# Se receber a versão no parametro, faz checkou
if [ "$1" != "" ]; then
	cd /home/wildfly/projetos/travel-corporate-java-api/

	git checkout --track -b origin/$1

	git checkout $1

	cd /home/wildfly/projetos/travel-corporate-app

	git checkout --track -b origin/$1

	git checkout $1
else
	cd /home/wildfly/projetos/travel-corporate-java-api/

	git pull

	cd /home/wildfly/projetos/travel-corporate-app

	git pull
fi